FROM mariadb:10.1

ENV MYSQL_ROOT_PASSWORD=pasword

# ADD sql scripts to docker image.
COPY db-data db


# Installing database client.
# RUN cp sql-scripts/create_schema.sql docker-entrypoint-initdb.d/
# RUN cp sql-scripts/create_users.sql docker-entrypoint-initdb.d/

RUN cd db && ./cat_scripts.sh
RUN cp db/create_all.sql docker-entrypoint-initdb.d/

ENTRYPOINT ["docker-entrypoint.sh"]

EXPOSE 3306
CMD ["mysqld"]
