#This is the demo for the presentation about Docker

Presentation is awaylable at:
https://karklas.mif.vu.lt/~mabl2102/psmi/docker/.

* Check "Makefile";
* Access applications:
  - Web app UI - http://localhost:9002/ppr-ui
  - Web app API - http://localhost:9001/ppr-api
  - Web app DB - SQL localhost:3306