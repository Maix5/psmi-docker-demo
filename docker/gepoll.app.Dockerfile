FROM tomcat:9-jre8

ARG GEPOLL_NAME="gepoll-frontend"
ADD gepoll-app/frontend/target/$GEPOLL_NAME*.war webapps/
RUN mv webapps/ROOT webapps/_ROOT

ARG DOWLOAD_LINK_JDBC="http://central.maven.org/maven2/mysql/mysql-connector-java/5.1.46/mysql-connector-java-5.1.46.jar"

# # Making logs dir ectory more acccesible:
# RUN mkdir /app-logs
# RUN rm -r logs
# RUN ln -srf /app-logs logs

# ADD gepoll-app/model/src/main/resources/sql/sqlite/* sql/
ADD gepoll-app/frontend/target/gepoll-frontend*.war webapps/

RUN apt-get update
# Installing database client.
RUN apt-get install -y wget
# Installing server JDBC client.
RUN wget -P lib/ -N $DOWLOAD_LINK_JDBC

## Applications for administrative work:
# Installing network tools.
RUN apt-get install -y net-tools
# Installing text editor.
RUN apt-get install -y vim
# Installing file explorer.
RUN apt-get install -y ranger

# Installing network tools.
# Adding all configuration files to docker image:
ADD docker/gepoll-app-data gepoll-app-data

# Changing working dir.
WORKDIR gepoll-app-data

############################################
# Specifying general environment variables #
############################################
# {{{1

ENV GEPOLL_NAME_ENV=$GEPOLL_NAME

ENV PORT_HTTP=8080
ENV PORT_HTTPS=8443

# }}}1

###################################################
# Specifying SSL encription environment variables #
###################################################
# This is used in tomcat "server.*.xml" and "web.*.xml" {{{1

ENV ENABLE_SSL_ENCRIPTION=false
ENV ENABLE_SSL_REDIRECTION=false

# These environment variables need to be set
# in order for HTTPS to work correctlly.


# If "Let's encript" service is used for SSL certificate generation,
# then this file can be located by this path:
# "/cert/live/<url-dir>/cert.pem"
ENV SSL_CERTIFICATE_FILE_PATH=
# If "Let's encript" service is used for SSL certificate generation,
# then this file can be located by this path:
# "/cert/live/<url-dir>/privkey.pem"
ENV SSL_CERTIFICATE_KEY_FILE_PATH=
# If "Let's encript" service is used for SSL certificate generation,
# then this file can be located by this path:
# "/cert/live/<url-dir>/chain.pem"
ENV SSL_CERTIFICATE_CHAIN_FILE_PATH=


# }}}1

########################################################
# Specifying application runtime environment variables #
########################################################
# This is used in direct application donfiguration {{{1

# Explanation for spring profiles:
#
# This project defines these profiles:
# dev, production, schedule, debug (more logging)
#
# Profiles that changes data layer persistence type (CHOOSE ONLY ONE):
#     * "persist-in-memory"
#     * "persist-in-memory-with-default"
#     * "persist-in-db" --- (requires additionally specify db type,
#       options bellow, CHOOSE ONLY ONE):
#         - "relational-db"
#         - "mongo-db"

# # Persistence in operating memoty is enabled.
# ENV APPLICATION_SPRING_PROFILES="production, persist-in-memory"
# Persistence in relational (SQL) database is enabled.
# ENV APPLICATION_SPRING_PROFILES="production, persist-in-db, relational-db"
# Persistence in Mongo (document) database is enabled.
ENV APPLICATION_SPRING_PROFILES="production, persist-in-db, mongo-db"

# }}}1

####################################################
# Specifying JDBC connection environment variables #
####################################################
# This is used in tomcat "context.xml" {{{1

ENV DB_CONNECTION_JDBC_RESOURCE_NAME="jdbc/gepoll_db"

ENV DB_CONNECTION_JDBC_HOST="gepoll-db-jdbc-server"
ENV DB_CONNECTION_JDBC_PORT="3306"

ENV DB_CONNECTION_JDBC_USERNAME="gepoll-user"
ENV DB_CONNECTION_JDBC_PASSWORD="gepoll-user"
ENV DB_CONNECTION_JDBC_SCHEMA="GEPOLL"

# }}}1

########################################################
# Specifying Mongo DB connection environment variables #
########################################################
# This is used in tomcat "context.xml" {{{1

ENV DB_CONNECTION_MONGODB_RESOURCE_NAME="mongo_db/gepoll_db"
ENV DB_CONNECTION_MONGODB_RESOURCE_TYPE_CLASSPATH="lt.teamMitSoft.gepoll.model.dao.inDb.mongoDb.cfg.DataSourceMongoDb"
ENV DB_CONNECTION_MONGODB_RESOURCE_CONSTRUCTOR_CLASSPATH="lt.teamMitSoft.gepoll.model.dao.inDb.mongoDb.cfg.DataSourceMongoDbJndiFactory"

ENV DB_CONNECTION_MONGODB_HOST="gepoll-db-mongo-server"
ENV DB_CONNECTION_MONGODB_PORT="27017"

ENV DB_CONNECTION_MONGODB_USERNAME="gepoll-user"
ENV DB_CONNECTION_MONGODB_PASSWORD="gepoll-user"
ENV DB_CONNECTION_MONGODB_DATABASENAME="GEPOLL"

# }}}1

# Because exposed port can cahange they are not specified here,
# one should explisitlly specify them with '--expose' flag
# during the startup of the container.
# More info: https://stackoverflow.com/a/50178593
# EXPOSE $PORT_HTTP
# EXPOSE $PORT_HTTPS


# Set scripts as executable.
RUN chmod +x init.sh

# Start application.
# CMD ./bin/startup.sh && bash
# CMD ["catalina.sh", "run"]
CMD ["./init.sh"]
