# This is docker file meant to create "GePoll" system database.
# It is based on Mongo DB and exposes port 27017.
# In order to link this container to other do:
#   docker run
#       --name some-app
#       --link some-mongo-db-container-name:link_name
#       -d
#       application-that-uses-link_name-as-ip

FROM mongo:4.0.6

ENV MONGO_INITDB_ROOT_USERNAME=root
ENV MONGO_INITDB_ROOT_PASSWORD=16gepoll86root17db

# ADD sql scripts to docker image.
ADD docker/gepoll-db-mongo-data/mongo-db-scripts scripts


# Installing database client.
# RUN cp sql-scripts/create_schema.sql docker-entrypoint-initdb.d/
# RUN cp sql-scripts/create_users.sql docker-entrypoint-initdb.d/

RUN cp scripts/* docker-entrypoint-initdb.d/


ENTRYPOINT ["docker-entrypoint.sh"]

EXPOSE 27017
CMD ["mongod"]
