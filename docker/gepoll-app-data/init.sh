#!/bin/sh

# This script is ment to inicialize the the application static files.
# Original information from: https://stackoverflow.com/a/23957805

# Installing new context.xml
cp context.xml ../conf/context.xml

# Memory limitations
SMEMORY=1G
XMEMORY=1G

# SSL encription.
if [ "$ENABLE_SSL_ENCRIPTION" = "true" ]; then
    echo "NOTE: SSL encription enabled!"
    # Installing new server.xml
    cp server.ssl.xml ../conf/server.xml
else
    echo "NOTE: SSL encription disabled!"
    # Installing new server.xml
    cp server.xml ../conf/server.xml
fi

# HTTPS redirect.
if [ "$ENABLE_SSL_REDIRECTION" = "true" ]; then
    echo "NOTE: SSL redirection enabled!"
    # Installing new web.xml
    cp web.redirection.xml ../conf/web.xml
else
    echo "NOTE: SSL redirection disabled!"
    # Installing new web.xml
    cp web.xml ../conf/web.xml
fi




GEPOLL_ARTIFACT_NAME=$(basename -s ".war" $(ls ../webapps/$GEPOLL_NAME_ENV*.war))

# Tranfer relevant environment variables to format
# acceptable by tomcat xml cfgs.
export CATALINA_OPTS="$CATALINA_OPTS \
    -Dgepoll.artifact.name=${GEPOLL_ARTIFACT_NAME} \
    \
    -d64 -server -Xms$SMEMORY -Xmx$XMEMORY \
    -XX:+UseCodeCacheFlushing -XX:ReservedCodeCacheSize=64M \
    -XX:+HeapDumpOnOutOfMemoryError -XX:MaxPermSize=1024M \
    \
    -Dport.http=${PORT_HTTP} \
    -Dport.https=${PORT_HTTPS} \
    \
    -Ddb.connection.jdbc.resource.name=${DB_CONNECTION_JDBC_RESOURCE_NAME} \
    -Ddb.connection.jdbc.userName=${DB_CONNECTION_JDBC_USERNAME} \
    -Ddb.connection.jdbc.password=${DB_CONNECTION_JDBC_PASSWORD} \
    -Ddb.connection.jdbc.host=${DB_CONNECTION_JDBC_HOST} \
    -Ddb.connection.jdbc.port=${DB_CONNECTION_JDBC_PORT} \
    -Ddb.connection.jdbc.schema=${DB_CONNECTION_JDBC_SCHEMA} \
    \
    -Ddb.connection.mongoDb.resource.name=${DB_CONNECTION_MONGODB_RESOURCE_NAME} \
    -Ddb.connection.mongoDb.resource.type.classPath=${DB_CONNECTION_MONGODB_RESOURCE_TYPE_CLASSPATH} \
    -Ddb.connection.mongoDb.resource.constructor.classPath=${DB_CONNECTION_MONGODB_RESOURCE_CONSTRUCTOR_CLASSPATH} \
    -Ddb.connection.mongoDb.host=${DB_CONNECTION_MONGODB_HOST} \
    -Ddb.connection.mongoDb.port=${DB_CONNECTION_MONGODB_PORT} \
    -Ddb.connection.mongoDb.userName=${DB_CONNECTION_MONGODB_USERNAME} \
    -Ddb.connection.mongoDb.password=${DB_CONNECTION_MONGODB_PASSWORD} \
    -Ddb.connection.mongoDb.databaseName=${DB_CONNECTION_MONGODB_DATABASENAME} \
    \
    -Dssl.certificate.file.path=${SSL_CERTIFICATE_FILE_PATH} \
    -Dssl.certificate.key.file.path=${SSL_CERTIFICATE_KEY_FILE_PATH} \
    -Dssl.certificate.chain.file.path=${SSL_CERTIFICATE_CHAIN_FILE_PATH} \
    \
    -Dspring.profiles.active='${APPLICATION_SPRING_PROFILES}' \
"
# # Setts spring profile globally:
# export SPRING_PROFILES_ACTIVE="${APPLICATION_SPRING_PROFILES}"

exec $CATALINA_HOME/bin/catalina.sh run
