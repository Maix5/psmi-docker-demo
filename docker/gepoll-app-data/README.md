# GEPOLL application configuration files

This repository contains configuration files for tomcat which will be placed
in docker image.

Files meannings:
* "context.xml" --- contains information necessary to connect to the DB.

* "init.sh" --- read environment variables and based on their values
  inicializes statically (by copying correct files to correct
  configuration places) tomcat and gepoll applications.
  Also retranslates environment variables to be readable
  in tomcat xml configs and start tomcat application server.

* "server.ssl.xml" --- cocnfigures connector for the tomcat application.
  Has neccesary configurations for enabling comunication via HTTPS
  (HTTP encripted by SSL) protocol.
* "server.xml" --- cocnfigures connector for the tomcat application.

* "web.redirection.xml" --- has configuration for activating redirection
  for HTTPS protocol, when connection is established via HTTP protocol.
* "web.xml"
