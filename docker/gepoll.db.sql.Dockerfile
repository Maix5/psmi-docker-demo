# This is docker file meant to create "GePoll" system database.
# It is based on MYSQL and exposes port 3306.
# In order to link this container to other do:
#   docker run
#       --name some-app
#       --link some-mariadb:mysql
#       -d
#       application-that-uses-mysql

FROM mariadb:10.1

ENV MYSQL_ROOT_PASSWORD=16gepoll86root17db
# ENV MYSQL_ROOT_PASSWORD=test


# ADD sql scripts to docker image.
ADD docker/gepoll-db-data/sql/mysql sql-scripts


# Installing database client.
# RUN cp sql-scripts/create_schema.sql docker-entrypoint-initdb.d/
# RUN cp sql-scripts/create_users.sql docker-entrypoint-initdb.d/

RUN cd sql-scripts && ./create_all.sh
RUN cp sql-scripts/init_db.sql docker-entrypoint-initdb.d/


ENTRYPOINT ["docker-entrypoint.sh"]

EXPOSE 3306
CMD ["mysqld"]
