FROM tomcat:9-jre8


ARG DOWLOAD_LINK_JDBC="http://central.maven.org/maven2/mysql/mysql-connector-java/5.1.46/mysql-connector-java-5.1.46.jar"

RUN apt-get update
# Installing database client.
RUN apt-get install -y wget
# Installing server JDBC client.
RUN wget -P lib/ -N $DOWLOAD_LINK_JDBC

# Copying application:
COPY app-api-data/ppr-api.war webapps/

# Start application.
CMD ["catalina.sh", "run"]
