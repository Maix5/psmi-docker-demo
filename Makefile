SHELL := /bin/bash

IMAGE_VERSION=1.1
DOCKER_REGISTRY=registry.gitlab.com
DOCKER_REPO_URL=maix5/psmi-docker-demo

DOCKER_IMAGE_NAME_APP_UI=$(DOCKER_REGISTRY)/$(DOCKER_REPO_URL)/app-ui:$(IMAGE_VERSION)
DOCKER_IMAGE_NAME_APP_API=$(DOCKER_REGISTRY)/$(DOCKER_REPO_URL)/app-api:$(IMAGE_VERSION)
DOCKER_IMAGE_NAME_DB=$(DOCKER_REGISTRY)/$(DOCKER_REPO_URL)/db:$(IMAGE_VERSION)

DOCKER_CONTAINER_NAME_APP_UI=app-ui
DOCKER_CONTAINER_NAME_APP_API=app-api
DOCKER_CONTAINER_NAME_DB=db

DEMO_NEWORK_NAME=demo-net

###############################################################################
# For convenience
###############################################################################

.PHONY: all_build
all_build: db_build app_api_build app_ui_build

.PHONY: all_run
all_run: db_run app_api_run app_ui_run

.PHONY: all_run_net
all_run_net: net_rm net_create db_run_net app_api_run_net app_ui_run_net

.PHONY: all_clear
all_clear: db_clear app_api_clear app_ui_clear

.PHONY: all_push
all_push: db_push app_api_push app_ui_push


###############################################################################
# Docker image build
###############################################################################

.PHONY: db_build
db_build:
	docker build \
		-t $(DOCKER_IMAGE_NAME_DB) \
		-f db.Dockerfile \
		.

.PHONY: app_api_build
app_api_build:
	docker build \
		-t $(DOCKER_IMAGE_NAME_APP_API) \
		-f app-api.Dockerfile \
		.

.PHONY: app_ui_build
app_ui_build:
	docker build \
		-t $(DOCKER_IMAGE_NAME_APP_UI) \
		-f app-ui.Dockerfile \
		.

###############################################################################
# Docker container run
###############################################################################

.PHONY: db_run
db_run:
	echo "Exposed web DB url 'localhost:9000'."
	docker run \
		-d \
		-p 9000:8080 \
		--name $(DOCKER_CONTAINER_NAME_DB) \
		$(DOCKER_IMAGE_NAME_DB)

.PHONY: app_api_run
app_api_run:
	echo "Exposed web app-api url 'localhost:9001'."
	docker run \
		-d \
		-p 9001:8080 \
		--link $(DOCKER_CONTAINER_NAME_DB):$(DOCKER_CONTAINER_NAME_DB) \
		--name $(DOCKER_CONTAINER_NAME_APP_API) \
		$(DOCKER_IMAGE_NAME_APP_API)

.PHONY: app_ui_run
app_ui_run:
	echo "Exposed web app-ui url 'localhost:9002'."
	docker run \
		-d \
		-p 9002:8080 \
		--link $(DOCKER_CONTAINER_NAME_APP_API):$(DOCKER_CONTAINER_NAME_APP_API) \
		--name $(DOCKER_CONTAINER_NAME_APP_UI) \
		$(DOCKER_IMAGE_NAME_APP_UI)





















.PHONY: db_run_net
db_run_net:
	echo "Exposed web DB url 'localhost:9000'."
	docker run \
		-d \
		-p 9000:8080 \
		--network $(DEMO_NEWORK_NAME) \
		--name $(DOCKER_CONTAINER_NAME_DB) \
		$(DOCKER_IMAGE_NAME_DB)

.PHONY: app_api_run_net
app_api_run_net:
	echo "Exposed web app-api url 'localhost:9001'."
	docker run \
		-d \
		-p 9001:8080 \
		--network $(DEMO_NEWORK_NAME) \
		--name $(DOCKER_CONTAINER_NAME_APP_API) \
		$(DOCKER_IMAGE_NAME_APP_API)

.PHONY: app_ui_run_net
app_ui_run_net:
	echo "Exposed web app-ui url 'localhost:9002'."
	docker run \
		-d \
		-p 9002:8080 \
		--network $(DEMO_NEWORK_NAME) \
		--name $(DOCKER_CONTAINER_NAME_APP_UI) \
		$(DOCKER_IMAGE_NAME_APP_UI)


###############################################################################
# Docker container clear
###############################################################################
.PHONY: db_clear
db_clear:
	docker stop $(DOCKER_CONTAINER_NAME_DB) || true
	docker rm $(DOCKER_CONTAINER_NAME_DB) || true

.PHONY: app_api_clear
app_api_clear:
	docker stop $(DOCKER_CONTAINER_NAME_APP_API) || true
	docker rm $(DOCKER_CONTAINER_NAME_APP_API) || true

.PHONY: app_ui_clear
app_ui_clear:
	docker stop $(DOCKER_CONTAINER_NAME_APP_UI) || true
	docker rm $(DOCKER_CONTAINER_NAME_APP_UI) || true



###############################################################################
# Docker image store to registry.
###############################################################################
.PHONY: db_push
db_push:
	docker login $(DOCKER_REGISTRY)
	docker push $(DOCKER_IMAGE_NAME_DB)
	docker logout

.PHONY: app_api_push
app_api_push:
	docker login $(DOCKER_REGISTRY)
	docker push $(DOCKER_IMAGE_NAME_APP_API)
	docker logout

.PHONY: app_ui_push
app_ui_push:
	docker login $(DOCKER_REGISTRY)
	docker push $(DOCKER_IMAGE_NAME_APP_UI)
	docker logout


###############################################################################
# Docker network
###############################################################################
.PHONY: net_create
net_create:
	docker network create \
		-d bridge \
		$(DEMO_NEWORK_NAME)

.PHONY: net_rm
net_rm:
	docker network rm \
		$(DEMO_NEWORK_NAME)
