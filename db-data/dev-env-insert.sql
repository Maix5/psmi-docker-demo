-- # User groups
USE post_pkg_reg;

INSERT INTO address
    (id, street_name, building_nr, apartment_nr, city_name, country_name)
    VALUES(1,'Street 1 st.', 1, 1, 'City 1', 'Country 1');
INSERT INTO address
    (id, street_name, building_nr, apartment_nr, city_name, country_name)
    VALUES(2,'Street 2 st.', 2, 2, 'City 2', 'Country 2');
INSERT INTO address
    (id, street_name, building_nr, apartment_nr, city_name, country_name)
    VALUES(3,'Street 3 st.', 3, 3, 'City 3', 'Country 3');
INSERT INTO address
    (id, street_name, building_nr, apartment_nr, city_name, country_name)
    VALUES(4,'Street 4 st.', 4, 4, 'City 4', 'Country 4');
INSERT INTO address
    (id, street_name, building_nr, apartment_nr, city_name, country_name)
    VALUES(5,'Street 5 st.', 5, 5, 'City 5', 'Country 5');
INSERT INTO address
    (id, street_name, building_nr, apartment_nr, city_name, country_name)
    VALUES(6,'Street 6 st.', 6, 6, 'City 6', 'Country 6');
INSERT INTO address
    (id, street_name, building_nr, apartment_nr, city_name, country_name)
    VALUES(7,'Street 7 st.', 7, 7, 'City 7', 'Country 7');
INSERT INTO address
    (id, street_name, building_nr, apartment_nr, city_name, country_name)
    VALUES(8,'Street 8 st.', 8, 8, 'City 8', 'Country 8');
INSERT INTO address
    (id, street_name, building_nr, apartment_nr, city_name, country_name)
    VALUES(9,'Street 9 st.', 9, 9, 'City 9', 'Country 8');
INSERT INTO address
    (id, street_name, building_nr, apartment_nr, city_name, country_name)
    VALUES(10,'Street 10 st.', 10, 10, 'City 10', 'Country 10');
