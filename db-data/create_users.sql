-- Create user sentences for test MONODBC database

-- Creates users accounts: (password can be changed accordingly)
CREATE USER 'post-pkg-reg-user' IDENTIFIED BY 'post-pkg-reg-user';



-- Granting privileges to the user:
GRANT CREATE TEMPORARY TABLES ON GEPOLL.*
    TO  'post-pkg-reg-user';

GRANT SELECT, INSERT, UPDATE, DELETE, CREATE, DROP,
INDEX, ALTER, CREATE TEMPORARY TABLES, CREATE VIEW
    ON post_pkg_reg.* TO  'post-pkg-reg-user';
