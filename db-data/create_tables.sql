-- Create table sentences for gepoll database
USE post_pkg_reg;

CREATE TABLE IF NOT EXISTS address (
    id INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT,
    street_name VARCHAR(1024) NOT NULL,
    building_nr INTEGER  NOT NULL,
    apartment_nr INTEGER NOT NULL,
    city_name VARCHAR(1024) NOT NULL,
    country_name VARCHAR(1024) NOT NULL
);

