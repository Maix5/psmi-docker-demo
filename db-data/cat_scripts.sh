#!/bin/bash

cat \
    create_schema.sql \
    create_users.sql \
    create_tables.sql \
    dev-env-insert.sql \
    > create_all.sql
